package calculator;


import static helper.ErrorMessages.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * Test class for {@link Calculator}
 */
public class CalculatorTest {

    @Test
    public void shouldThrowExceptionForNullString() {
        try {
            //when
            Calculator.add(null);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(NULL_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldReturnZeroForEmptyString() throws Exception {
        //when
        Integer result = Calculator.add(StringUtils.EMPTY);
        //then
        assertEquals(Integer.valueOf(0), result);
    }

    @Test
    public void shouldReturnCorrectResultForSingleNumber() throws Exception {
        //given
        String numbers = "1";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void shouldThrowExceptionForSingleNumberThatIsNotANumber() {
        //given
        String numbers = "a";
        try {
            //when
            Calculator.add(numbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(WRONG_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldReturnCorrectResultForTwoNumbersWithComma() throws Exception {
        //given
        String numbers = "1,2";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(3), result);
    }

    @Test
    public void shouldThrowExceptionForFirstArgumentNotNumber() {
        //given
        String numbers = "a,2";
        try {
            //when
            Calculator.add(numbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(WRONG_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionForSecondArgumentNotNumber() {
        //given
        String numbers = "1,a";
        try {
            //when
            Calculator.add(numbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(WRONG_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldReturnCorrectResultForAnyAmountOfNumbersWitComma() throws Exception {
        //given
        String numbers = "0,1,2,3,4,5";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(15), result);
    }

    @Test
    public void shouldReturnCorrectResultForAnyAmountOfNumbersWitNewline() throws Exception {
        //given
        String numbers = "0,1\n2,3\n4,5";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(15), result);
    }

    @Test
    public void shouldThrowExceptionForNewlineInsteadOfNumber() {
        //given
        String numbers = "0,1,2,3,\n,5";
        try {
            //when
            Calculator.add(numbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(WRONG_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldThrowExceptionForUnsupportedDelimiter() {
        //given
        String numbers = "0.1,2,3,\n,5";
        try {
            //when
            Calculator.add(numbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(WRONG_ARGUMENT, e.getMessage());
        }
    }

    @Test
    public void shouldReturnCorrectResultForCustomDelimiter() throws Exception {
        //given
        String numbers = "//;\n1;2;3";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(6), result);
    }

    @Test
    public void shouldReturnCorrectResultForLongerCustomDelimiter() throws Exception {
        //given
        String numbers = "//[***]\n1***2***3***4";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(10), result);
    }

    @Test
    public void shouldThrowExceptionForNegativeNumber() {
        //given
        String negativeNumbers = "0,-1,2,-3";
        try {
            //when
            Calculator.add(negativeNumbers);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(String.format(NEGATIVES_NOT_ALLOWED, "[-1, -3]"), e.getMessage());
        }
    }

    @Test
    public void shouldIgnoreNumberBiggerThanThousand() throws Exception {
        //given
        String numbers = "2,1001,1";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(3), result);
    }

    @Test
    public void shouldReturnCorrectResultForManySingleCharacterCustomDelimiters() throws Exception {
        //given
        String numbers = "//[*][%]\n1*2%3*4%5";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(15), result);
    }

    @Test
    public void shouldReturnCorrectResultForManyLongerCustomDelimiters() throws Exception {
        //given
        String numbers = "//[*&^][%%][$$]\n1*&^2$$3%%4$$5";
        //when
        Integer result = Calculator.add(numbers);
        //then
        assertEquals(Integer.valueOf(15), result);
    }

}