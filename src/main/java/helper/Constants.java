package helper;

/**
 * Auxiliary class for holding application constants f.e. default delimiters
 */
public class Constants {
    public static final String DELIMITER_COMMA = ",";
    public static final String DELIMITER_NEWLINE = "\n";
    public static final String CUSTOM_DELIMITER_MARKER = "//";
    public static final Integer ARGUMENT_VALUE_LIMIT = 1000;
    public static final Character CUSTOM_DELIMITER_OPENING_BRACKET = '[';
    public static final Character CUSTOM_DELIMITER_CLOSING_BRACKET = ']';
}
