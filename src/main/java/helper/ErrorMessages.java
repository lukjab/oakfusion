package helper;

/**
 * Auxiliary class for holding application error messages
 */
public class ErrorMessages {

    public static final String NULL_ARGUMENT = "Provided numbers can't be null";
    public static final String WRONG_ARGUMENT = "One or more of provided number is not a number, or provided delimiter is not supported";
    public static final String NEGATIVES_NOT_ALLOWED = "Negatives not allowed. You provided negative values: %s";
}
