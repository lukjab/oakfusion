package calculator.validator;

import static helper.ErrorMessages.NEGATIVES_NOT_ALLOWED;
import static helper.ErrorMessages.NULL_ARGUMENT;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Auxiliary class used for performing validations
 */
public class Validator {

    public static void validateNegatives(List<Integer> numbers) throws Exception {
        List<Integer> negatives = numbers.stream()
                .filter(number -> number < 0)
                .collect(Collectors.toList());

        if (!negatives.isEmpty()) {
            throw new Exception(String.format(NEGATIVES_NOT_ALLOWED, negatives.toString()));
        }
    }

    public static void validateNullArgument(String numbers) throws Exception {
        if (numbers == null) {
            throw new Exception(NULL_ARGUMENT);
        }
    }
}
