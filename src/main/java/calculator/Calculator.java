package calculator;

import static calculator.validator.Validator.validateNegatives;
import static calculator.validator.Validator.validateNullArgument;
import static helper.Constants.*;
import static helper.ErrorMessages.WRONG_ARGUMENT;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

/**
 * Class holding solution to required task
 */
public class Calculator {

    public static Integer add(String numbers) throws Exception {

        validateNullArgument(numbers);

        if (StringUtils.EMPTY.equals(numbers)) {
            return 0;
        }

        List<String> numbersAsStrings = splitNumbersByDelimiter(numbers);
        List<Integer> numbersAsIntegers;

        try {
            numbersAsIntegers = convertStringsToIntegers(numbersAsStrings);
        } catch (NumberFormatException e) {
            throw new Exception(WRONG_ARGUMENT);
        }

        validateNegatives(numbersAsIntegers);

        if (numbersAsIntegers.size() == 1) {
            return numbersAsIntegers.get(0);
        } else {
            return numbersAsIntegers.stream()
                    .mapToInt(Integer::intValue).sum();
        }
    }

    private static List<String> splitNumbersByDelimiter(String numbers) {
        Boolean isCustomDelimiterUsed = numbers.startsWith(CUSTOM_DELIMITER_MARKER);

        if (isCustomDelimiterUsed) {
            return splitNumbersByCustomDelimiter(numbers);
        }

        return splitNumbersByCommaAndNewline(numbers);
    }

    private static List<String> splitNumbersByCustomDelimiter(String numbers) {
        Boolean isDelimiterInBrackets = numbers.charAt(CUSTOM_DELIMITER_MARKER.length()) == CUSTOM_DELIMITER_OPENING_BRACKET;

        if (isDelimiterInBrackets) {
            return splitNumbersByMultipleCustomDelimiters(numbers);
        }

        String otherDelimiter = numbers.substring(CUSTOM_DELIMITER_MARKER.length(), numbers.indexOf(DELIMITER_NEWLINE));
        String numbersAfterDelimiter = numbers.substring(numbers.indexOf(DELIMITER_NEWLINE) + DELIMITER_NEWLINE.length());

        return getArrayList(numbersAfterDelimiter, otherDelimiter);
    }

    private static List<String> splitNumbersByCommaAndNewline(String numbers) {
        Boolean isCommaDelimiterUsed = numbers.contains(DELIMITER_COMMA);
        Boolean isNewLineDelimiterUsed = numbers.contains(DELIMITER_NEWLINE);

        if (isCommaDelimiterUsed && isNewLineDelimiterUsed) {
            String numbersWithUnifiedCommaDelimiter = numbers.replace(DELIMITER_NEWLINE, DELIMITER_COMMA);

            return getArrayList(numbersWithUnifiedCommaDelimiter, DELIMITER_COMMA);
        } else if (!isCommaDelimiterUsed && isNewLineDelimiterUsed) {
            return getArrayList(numbers, DELIMITER_NEWLINE);
        } else {
            return getArrayList(numbers, DELIMITER_COMMA);
        }
    }

    private static List<String> getArrayList(String numbers, String delimiter) {
        return Arrays.asList(numbers.split(Pattern.quote(delimiter)));
    }

    private static List<String> splitNumbersByMultipleCustomDelimiters(String numbers) {
        String delimitersString = numbers.substring(CUSTOM_DELIMITER_MARKER.length(), numbers.indexOf(DELIMITER_NEWLINE));
        String numbersString = numbers.substring(numbers.indexOf(DELIMITER_NEWLINE) + DELIMITER_NEWLINE.length());
        List<String> delimiters = getDelimitersFromString(delimitersString);

        String unifiedDelimiter = delimiters.get(0);

        for (String delimiter : delimiters) {
            if (!delimiter.equals(unifiedDelimiter)) {
                numbersString = numbersString.replace(delimiter, unifiedDelimiter);
            }
        }

        return getArrayList(numbersString, unifiedDelimiter);
    }

    private static List<String> getDelimitersFromString(String multipleDelimitersString) {
        List<String> delimiters = getArrayList(multipleDelimitersString, CUSTOM_DELIMITER_CLOSING_BRACKET.toString());

        return delimiters.stream()
                .map(delimiter -> delimiter.substring(1))
                .collect(Collectors.toList());
    }

    private static List<Integer> convertStringsToIntegers(List<String> numbersAsStrings) {
        return numbersAsStrings.stream()
                .map(Integer::parseInt)
                .filter(number -> number <= ARGUMENT_VALUE_LIMIT)
                .collect(Collectors.toList());
    }

}
